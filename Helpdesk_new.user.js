// ==UserScript==
// @name Helpdesk new
// @namespace Helpdesk
// @match https://helpdesk.efko.ru/*
// @grant none
// @version     2.0
// ==/UserScript==

let timeUpdate = 60000;

window.addEventListener("load", function(event) {
  if (location.href.indexOf('user_detail_tasks.php') == -1)
    return;
  setInterval(function() {
    location.reload();
  }, timeUpdate);
});
